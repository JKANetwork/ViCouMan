<?php
//Primero conectar al servidor
$conexion = mysqli_connect("localhost","user","password","database(default vicouman)") //host,user,pass,basededatos
or die ("Fallo en el establecimiento de la conexión");

//Verificación del usuario doble cookie para evitar trampas en la cookie de login
if (isset($_COOKIE["loguer"]))
{
    $sql = sprintf("SELECT * FROM usuarios WHERE ID = '" .$_COOKIE['loguer']. "'");
    $consultauser = mysqli_query($conexion, $sql);
    $user = mysqli_fetch_array($consultauser);
	if ($user['sesid'] != $_COOKIE['sesid'])
	{
		setcookie("loguer", "", time()-3600);
		setcookie("sesid", "", time()-3600);
		echo "ERROR";
		?>
			<script type='text/javascript'>
   top.location = "index.php"; 
</script>
<?php
		die();
	}
}
//Fin verificación del usuario con doble cookie

function basicboot() //Esta funcion inicia html y arranca los estilos
{
?>
	<!DOCTYPE html>
	<html>
	<head>
	<title>Vicouman</title>
	<link rel="stylesheet" type="text/css" href="style.css"> 
		<link href='https://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'> <!-- Letra -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script> <!--Script para hacer grafico -->
		<meta name="description" content="Vicouman es tu nuevo contador de visitas simple, rapido, y gratuito!">
		<meta name="keywords" content="Vicouman,contador,visitas,web,usuarios,gratis">
		<!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
<script type="text/javascript">
    window.cookieconsent_options = {"message":"Esta web utiliza Cookies para mejorar la experiencia web. Navegando en ella aceptas esto.","dismiss":"Aceptar","learnMore":"Más información","link":null,"theme":"light-floating"};
</script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
<!-- End Cookie Consent plugin -->

	</head>
	<body>
	<iframe src="https://vicouman.com/contadorsc.php?webpage=3&estilo=0" style="width:1px;height:1px;border:0px;overflow:hidden"></iframe> <!-- Que menos que usar el contador en nuestra web xd -->
		<?php
}

function headmenu()
{
    echo "<p id='title'>ViCouMan<sup>beta</sup></p>";//Título
	if (!isset($_COOKIE['loguer'])) //Menú de persona sin loguear
	{
	?>
	<ul id="menuhor">
  <li><a href="index.php">Inicio</a></li>
  <li><a href="registrar.php">Registrarse</a></li>
  <li><a href="loginuser.php">Entrar</a></li>
  <li><a href="about.php">Acerca de</a></li>
  <li><a href="contrib.php">Ayudanos</a></li>
	</ul>
	<?php
	}
    if (isset($_COOKIE['loguer'])) //Menú de persona logueada
	{
	?>
	<ul id="menuhor">
  <li><a href="index.php">Inicio</a></li>
  <li><a href="panel.php">Panel</a></li>
  <li><a href="newpage.php">Añadir web</a></li>
  <li><a href="logout.php">Salir</a></li>
  <li><a href="about.php">Acerca de</a></li>
  <li><a href="contrib.php">Ayudanos</a></li>
	</ul>
	<?php
	}
}

function requirelogin()
{
	if (!isset($_COOKIE['loguer']))
	{
	?>
	<script type='text/javascript'>
    top.location = "index.php"; 
	</script>
	<?php
		die();
	}
}

function RandomString($length)
{
	//https://phpes.wordpress.com/2007/06/12/generador-de-una-cadena-aleatoria/
    $source = 'abcdefghijklmnopqrstuvwxyz';
    $source .= '1234567890';
    if($length>0){
        $rstr = "";
        $source = str_split($source,1);
        for($i=1; $i<=$length; $i++){
            mt_srand((double)microtime() * 1000000);
            $num = mt_rand(1,count($source));
            $rstr .= $source[$num-1];
        }

    }
    return $rstr;
}

?>