function loadcounter() { //Todo el js
	if (typeof pause == 'undefined'){ //Pause not defined
	pause=0;
	}
	
	var cookiename = "vicouman" + idweb;
	var b = document.cookie.match('(^|;)\\s*' + cookiename + '\\s*=\\s*([^;]+)');
	var iscookie = b ? b.pop() : '';
	if (iscookie != ""){
		var visited = 1; //Visitado
	}else{ //Si no esta visitado, hacer la cookie pero contar visita unica
		var visited = 0;
		var d = new Date();
		d.setTime(d.getTime() + (3*24*60*60*1000));
		var expires = "expires="+ d.toUTCString();
		document.cookie = "vicouman" + idweb + "=visited" + "; " + expires;
	}


	// Enviamos primero los datos para introducir en la DB
	// Obtener la instancia del objeto XMLHttpRequest
	conexion = new XMLHttpRequest();
	// Preparar la funcion de respuesta
	conexion.onreadystatechange = mostrar; //Cuando los introducza, recogemos lo nuevo.
	// Realizar peticion HTTP
	conexion.open('POST', 'https://vicouman.com/counterajax.php');
	conexion.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	conexion.send("idweb="+idweb+"&accid="+accid+"&mostrarU="+mostrarU+"&pause="+pause+"&visited="+visited);


	function mostrar() {
	  if(conexion.readyState == 4 && conexion.status == 200) {
		DivContador = document.getElementById('divVicouman');
		DivContStyle = document.getElementById('divVicouman').style;
		DivContStyle.display="inline";
		DivContStyle.textDecoration="none";
		switch (estilo){
			case 1:
			loadcssfile("https://fonts.googleapis.com/css?family=Roboto");
			DivContStyle.fontFamily="'Roboto', sans-serif";
			DivContStyle.fontSize="14px";
			DivContStyle.backgroundColor="black";
			DivContStyle.color="white";
			DivContStyle.margin="0.5px";
			break;
			case 2:
			loadcssfile("https://fonts.googleapis.com/css?family=Roboto");
			DivContStyle.fontFamily="'Roboto', sans-serif";
			DivContStyle.fontSize="14px";
			DivContStyle.backgroundColor="white";
			DivContStyle.color="black";
			DivContStyle.margin="0.5px";
			break;
			case 3:
			DivContStyle.fontFamily="monospace";
			DivContStyle.fontSize="12px";
			DivContStyle.backgroundColor="black";
			DivContStyle.color="white";
			DivContStyle.margin="0.5px";
			break;
			case 4:
			loadcssfile("https://fonts.googleapis.com/css?family=Orbitron");
			DivContStyle.fontFamily="'Orbitron', sans-serif";
			DivContStyle.fontSize="14px";
			DivContStyle.backgroundColor="white";
			DivContStyle.color="black";
			DivContStyle.margin="0px";
			break;
			case 5:
			loadcssfile("https://fonts.googleapis.com/css?family=Orbitron");
			DivContStyle.fontFamily="'Orbitron', sans-serif";
			DivContStyle.fontSize="14px";
			DivContStyle.backgroundColor="white";
			DivContStyle.color="green";
			DivContStyle.margin="0px";
			break;
			case 6:
			loadcssfile("https://fonts.googleapis.com/css?family=Indie+Flower");
			DivContStyle.fontFamily="'Indie Flower', sans-serif";
			DivContStyle.fontSize="18px";
			DivContStyle.backgroundColor="white";
			DivContStyle.color="black";
			DivContStyle.margin="0px";
			break;
			case 7:
			loadcssfile("https://fonts.googleapis.com/css?family=Source+Sans+Pro:600italic");
			DivContStyle.fontFamily="'Source Sans Pro', sans-serif";
			DivContStyle.fontSize="18px";
			DivContStyle.fontStyle="italic";
			DivContStyle.backgroundColor="white";
			DivContStyle.color="#ef4cf4";
			DivContStyle.margin="0px";
			break;
			case 8:
			DivContStyle.fontFamily="Courier,monospace";
			DivContStyle.fontSize="18px";
			DivContStyle.backgroundColor="white";
			DivContStyle.color="black";
			DivContStyle.margin="0px";
			break;
		}

			if (estilo != "0"){
			DivContador.innerHTML = "<a style='text-decoration:none;color:inherit' href='https://vicouman.com'>"+conexion.responseText+"</a>";
			}
		}
	}

}
loadcounter(); //Para que aparezca

function loadcssfile(filename) {
		var fileref = document.createElement("link")
		fileref.setAttribute("rel", "stylesheet")
		fileref.setAttribute("type", "text/css")
		fileref.setAttribute("href", filename)
}