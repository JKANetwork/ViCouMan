<?php
require "basic.php"; //Requerido el basic, que incluye la conexion a la base de datos y sus funciones
basicboot();
headmenu();
requirelogin();
//En la web, la variable $web['huso'] se la resta 1 porque el huso horario que usa el servidor es +1, asi se compensa
$web = mysqli_fetch_array(mysqli_query($conexion, "SELECT * FROM webs WHERE ID='$_GET[webpage]' AND iduser='$_COOKIE[loguer]'"));
?>
<div id="whitediv" style="width:100%;">
    <ul id="menue">
	  <li><a href="insertcode.php?webpage=<?php print $_GET[webpage]; ?>">Tu contador</a>
      <li><a href="estadisticas.php?webpage=<?php print $_GET[webpage]; ?>">Estadística lineal</a>
	  <li><a href="estadisticas.php?webpage=<?php print $_GET[webpage]; ?>&stl=2">Estadísticas en barras</a>
      <li><a href="editpage.php?webpage=<?php print $_GET[webpage]; ?>">Editar datos de la web</a>
	  <li><a href="opcweb.php?webpage=<?php print $_GET[webpage]; ?>">Opciones de la web</a>
    </ul>
	
	<p style="text-align:center;">Estadísticas de la web <?php print $web['nombreweb'] ?></p>


<div style="display: block;margin: 0 auto;width:800px;height:420px;"><canvas id="estgraf" style="display: block;margin: 0 auto"></canvas></div>
	
<p style="text-align:center;">General</p>
	
<table style="margin: 0 auto;"><tr><td style="vertical-align:top;"> <!-- Tabla para meter las tablas dentro -->
	
<table id="TbEst">
	<tr>
		<th>Día</th>
		<th style="min-width:100px;">Visitas</th>
		<th>Visitas unicas</th>
	</tr>
	<tr>
		<td>Siempre</td>
		<td><?php print $web['vistotales'] ?></td>
		<td><?php print $web['visUtotales'] ?></td>
	</tr>
		<tr>
		<td>Este mes</td>
		<td><?php print $web['vismes'] ?></td>
		<td><?php print $web['visUmes'] ?></td>
	</tr>
	<tr>
		<td>Hoy</td>
		<td><?php print $web['vishoy'] ?></td>
		<td><?php print $web['visUhoy'] ?></td>
	</tr>
	<?php
	$visdias = explode (",",$web['vishistorial']); //Dividir estadísticas de todos los días
	$visUdias = explode (",",$web['visUhistorial']); //Dividir estadísticas de todos los días
	$cantidad = count($visdias);
	reset($visdias); //Reseteamos los array
	reset($visUdias);
for ($i = 1; $i <= $cantidad; $i++){ //Como los dos array son iguales, duran lo mismo. While . se usa el contador - 1 porque los array empiezan en 0
	print "<tr>";
	print "<td>".date('d/m', strtotime('-'.($i).' day +'.$web[huso].'hour'))."</td>"; //Arreglos para que salga el día correcto
	print "<td>".$visdias[($i-1)]."</td>";
	print "<td>".$visUdias[($i-1)]."</td>";
	print "</tr>";
}
//Esta parte del código es para calcular los días en orden correcto para la grafica 
for ($i = ("-" . $cantidad); $i <= -1 ; $i++){ //Este for crea el array con los días en orden "real" para que la gráfica los muestre
	$arraycdias .= '"' .date('d/m', strtotime(($i).' day +'.($web[huso] -1).'hour')) . '",';
}
$arraycdias .= '"' .date('d/m', strtotime('now +'.$web[huso].'hour')). '"'; //Hay que añadir después de todo el día de hoy

$estgrafvisitas = implode(",",array_reverse($visdias)); //Dar la vuelta al array para que vaya de atras hacia alante, y añadir comas
$estgrafUvisitas = implode(",",array_reverse($visUdias));
//Fin Esta parte del código es para calcular los días en orden correcto para la grafica 

?>
	</table>
	
	</td><td style="vertical-align:top;"> <!-- Tabla para meter tablas dentro -->
	
		
<table id="TbEst"> <!-- Tabla meses -->
	<tr>
		<th>Mes</th>
		<th style="min-width:100px;">Visitas</th>
		<th>Visitas unicas</th>
	</tr>
		<tr>
		<td>Este mes</td>
		<td><?php print $web['vismes'] ?></td>
		<td><?php print $web['visUmes'] ?></td>
	</tr>
	<?php
	$vismeshist = explode (",",$web['vismeshist']); //Dividir estadísticas de todos los días
	$visUmeshist = explode (",",$web['visUmeshist']); //Dividir estadísticas de todos los días
	$cantidad = count($vismeshist);
	reset($vismeshist); //Reseteamos los array
	reset($visUmeshist);
for ($i = 1; $i <= $cantidad; $i++){ //Como los dos array son iguales, duran lo mismo. While . se usa el contador - 1 porque los array empiezan en 0
	print "<tr>";
	print "<td>".date('m', strtotime('first day of -'.($i).' month +'.$web[huso].'hour'))."</td>"; //Arreglos para que salga el mes correcto
	print "<td>".$vismeshist[($i-1)]."</td>";
	print "<td>".$visUmeshist[($i-1)]."</td>";
	print "</tr>";
}
?>
	</table>
	
<?php if ($web['opc_seeuptime'] == 1){ 	?>
	<br>Ultimos diez periodos de downtime<br>
	<table id="TbEst"> <!-- Tiempos de uptime -->
	<tr>
		<th>Fecha</th>
		<th style="min-width:100px;">Duración (minutos)</th>
	</tr>
	<?php
	$arrayuptime = explode(",",$web['seeuptime_downtime']);
	$cont =  (count($arrayuptime))-1;
	if ($cont == 0){
		$cont = -1;
	print "<tr>";
	print "<td colspan=2>No hay downtime registrado</td>";
	print "</tr>";	   
	}
	for ($x = 0;$x <= $cont;$x = $x+3)   { //Datos de cada vez
	
	print "<tr>";
	print "<td>".date('m-d h:i', $arrayuptime[$x] + $web['huso']*3600). "</td>"; //Arreglos para que salga el mes correcto
	if ($arrayuptime[$x+1] == 1){print "<td>Menos de 10</td>";}
	if ($arrayuptime[$x+1] != 1){print "<td>". (($arrayuptime[$x+1] * 10)-10) ."</td>";}
	print "</tr>";

}
echo "</table>"; //Tabla para meter tablas dentro
} 
?>
	
	</td><td style="vertical-align:top;"> <!-- Tabla para meter tablas dentro -->
	
<?php
if ($web['opc_browserest'] == 1){
	$arrayest = explode(",",$web['browserest']);
	echo "Estadísticas por navegadores (%)";
echo '<canvas id="browserpie" width="250" height="250" style="display: block;margin: 0 auto;"></canvas> <!-- Grafica navegadores -->';
}
if ($web['opc_seeuptime'] == 1){
	echo "Estadística de uptime este mes(%)";
echo '<canvas id="seeuptime" width="250" height="250" style="display: block;margin: 0 auto;"></canvas> <!-- Grafica uptime -->';
}
?>
	</td></tr></table>  <!-- Fin de la tabla para las tablas de estadisticas -->
	
		 <script>
    var ctx = document.getElementById("estgraf").getContext("2d");
    var myChart = new Chart(ctx, {
	type: '<?php print tipografica($_GET['stl']) ?>',
	  data: {
      labels: [<?php print $arraycdias;?>],
      datasets: [{
        label: "Visitas", 
        backgroundColor: "rgba(100,176,243,<?php print sombragrafica($_GET['stl']) ?>)", /* Color del sombreado de la grafica en ese lugar*/
        borderColor: "#4ba4f3", /* Colores de la linea y punteado */
        pointBorderColor: "#4ba4f3",
        pointBackgroundColor: "#fff",
        data: [<?php print $estgrafvisitas  . "," . $web['vishoy'];?>]
      },{
        label: "Visitas unicas",
        backgroundColor: "rgba(70,121,245,<?php print sombragrafica($_GET['stl']) ?>)", /* Color del sombreado de la grafica en ese lugar*/
        borderColor: "#2661f1",/* Colores de la linea y punteado */
        pointColor: "#2661f1",
		pointBorderColor: "#2661f1",
 		pointBackgroundColor: "#fff",
        data: [<?php print $estgrafUvisitas  . "," . $web['visUhoy']; ?>]
      }]
    }
	});
  </script>

<?php	

function tipografica($var){
	if ($var == '' || $var == '1') { return ("line");}
	if ($var == '2') { return ("bar");}
}
function sombragrafica($var){
	if ($var == '' || $var == '1') { return ("0.2");}
	if ($var == '2') { return ("1");}
}

 
if ($web['opc_browserest'] == 1){ 
	$cont =  (count($arrayest))-1;
	for ($m = 0;$m <= $cont;$m++)   {$totalnumero = $totalnumero + $arrayest[$m]; } //Sabemos el numero de la cantidad de datos que hay
		 for ($m = 0;$m <= $cont;$m++)	 {$arrayporcentaje[$m] = round((($arrayest[$m] * 100)/$totalnumero),2);} //Sacamos los porcentajes de cada uno
	?>
	
    <script>
    var ctxa = document.getElementById("browserpie").getContext("2d");
    var chartupt = new Chart(ctxa, {
	type: 'pie',
	data: {
	labels: ["Otros","Móvil","IE<9","Edge","IE11","Opera","Firefox","Chrome","Safari","Crawlers/Bots/Maquinas"],
	datasets: [
		{
			data: [<?php for ($i = 0;$i <=10;$i++){print $arrayporcentaje[$i];if ($i != "9"){print ",";}}?>],
		backgroundColor: ["#aeaeae","#169335","#3ebfd3","#18add5","#3ebfd3","#c01010","#db3d10","#aed515","#71e1bf","#151515"],
		hoverBackgroundColor: ["#b1b1b1","#1ba23c","#6acede","#32c5ed","#6acede","#d13939","#e45026","#bfe42c","#82eacb","#1f1f1f"]
	}]
	},
	options: {legend: {display: false}},
	});
  </script>
	
<?php }; 


if ($web['opc_seeuptime'] == 1){ 
	//$percentpie = array(0,0);; //Uptime, Downtime
	$arraypercent = explode(",",$web['seeuptime_stats']);
	?>
	
	    <script>
    var ctxupa = document.getElementById("seeuptime").getContext("2d");
    var chartupa = new Chart(ctxupa, {
	type: 'pie',
	data: {
	labels: ["Uptime","Downtime"],
	datasets: [
		{
			data: [<?php print round((($arraypercent[0]/($arraypercent[0]+$arraypercent[1]))*100),2) . "," . round((($arraypercent[1]/($arraypercent[0]+$arraypercent[1]))*100),2); ?>],
		backgroundColor: ["#35dd5d","#f10101"],
		hoverBackgroundColor: ["#53f179","#f53c3c"]
		}],
	}
	});
  </script>
	
	
<?php }; 
