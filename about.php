<?php
require("basic.php"); //Requerido el connect, que incluye la conexion a la base de datos y sus funciones
basicboot();
headmenu();
?>
<div id="whitediv">
	<p>Vicouman es el intento de crear un contador de visitas con estadísticas simples para que cualquier escritor/blogger/webmaster/... pueda tener su propio contador en menos de un minuto<br>
		Además, no recogemos más datos de los necesarios, solo tenemos nombres y correos de los usuarios registrados, y el contador utiliza una Cookie para saber si ya se ha visitado la web, no registra IP de usuarios en su web ni datos que no se necesiten,siendo más seguro para la privacidad del usuario. Opcionalmente damos opción de recoger algún dato estadístico como saber porcentajes de usuarios que te visitan desde el movil o navegador, opcional y sin ser rastreable hacia usuarios concretos, respetamos la privacidad del usuario
	</p>
	<p>Nos encantaría que cualquier idea o sugerencia nos la mandéis por correo electrónico a <a class="hrefsincol" href="mailto:contacto@jkanetwork.com">contacto@jkanetwork.com</a> con el asunto Vicouman</p>
	<br>
	Recursos externos utlizados a los que damos créditos:
	<br>
	Fondo de la web: <a class="hrefsincol" href="http://www.freepik.es/vector-gratis/fondo-de-textura-de-lino-gris_835399.htm">Diseñado por Freepik</a>
	<br>
	<a class="hrefsincol" href="http://www.chartjs.org">Chart.js</a>, librería para hacer las gráficas
	<br>
	Saber datos estadísticos sobre el navegador web, editado y usado de forma opcional <a class="hrefsincol" href="http://ejemplocodigo.com/ejemplo-php-detectar-navegador-de-los-visitantes/">http://ejemplocodigo.com/ejemplo-php-detectar-navegador-de-los-visitantes/</a>
	<br>
	<a class="hrefsincol" href="http://www.w3schools.com/">w3schools</a>, la mejor web de recursos HTML
	Diversos blogs de internet que hacen más fácil aprender
</div>