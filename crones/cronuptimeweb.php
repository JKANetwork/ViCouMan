<?php
$conexion = mysqli_connect("localhost","user","password","database(default vicouman)") //host,user,pass,basededatos
or die ("Fallo en el establecimiento de la conexión");

/** Funciones */

function enviarmail($url, $destinatario, $arg) {
	//para el envío en formato HTML 
	$headers = "MIME-Version: 1.0\r\n"; 
	$headers .= "Content-type: text/html; charset=utf-8\r\n"; 
	//dirección del remitente 
	$headers .= "From: Vicouman <correo@cambiame.com>\r\n"; 
	
	switch ($arg) {
		case 1:
			$title = $url . " is down";
			$textt = "La web " .$url. " esta fuera de servicio (Downtime)";
			break;
		case 2:
			$title = $url . " is up";
			$textt = "La web " .$url. " vuelve a estar disponible";
			break;
	}
	
	$cuerpo = '
	<head>
	<link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet" type="text/css">
	<style>
	h1 {
	  text-align: center;
	}
	p {
	  text-align: center;
	  font-family: sans-serif;
	}
	</style>
	</head>
	<body>
	<p style="font-family: Raleway; font-size: 3em">ViCouMan</p>
	<div style="background: #FFF; height: 100%">
	</br></br>
	<h1>Aviso</h1>
	<p>'.$textt.'</p>
	</div>
	</body>
	';
	mail($destinatario,$title,$cuerpo,$headers); //Funcion mail de PHP
}

/** Esto recortara el string si lleva mas de $c días contados, para evitar strings infinitos */
function limitecontar($string,$c) { 
	$arra = explode (",",$string);
	$res = count($arra);
	if ($arra[($res-1)] == "") //Si hay un array nulo al final (Es decir, una , sin nada, quitarla)
	{
		unset($arra[($res-1)]);
		$res--;
	}
	if ($res > $c) //Si es mayor de $c posiciones, borrar los necesarios para que solo salgan 4
	{
		array_splice($arra,$c); //Borrar array desde la posicion $c, ya que los más viejos están al final
	}
	$string = implode(",",$arra);
	return $string;
}

/** Esta funcion devuelve el estado de una url, modificado de http://php.net/manual/es/book.curl.php#102885 */
function http_status($url, $wait = 3)
{
	$time = microtime(true);

		// we are the parent
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, TRUE);
		curl_setopt($ch, CURLOPT_NOBODY, TRUE); // remove body
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_TIMEOUT, $wait); //timeout in seconds
		$head = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		if(!$head)
		{
			return FALSE;
		}else{
			return $httpCode;	
		}
} 
/** Funciones */


/** Proceso */
$tablaid = mysqli_query($conexion, "SELECT `ID` FROM `webs` WHERE `opc_seeuptime` = '1'"); //Consigo los ID de todas las webs que esten en el huso horario en el que ahora son las 0
while ($ejecucion = mysqli_fetch_array($tablaid, MYSQLI_NUM)) { //Cada web,comprobar si esta operativa y mandar mensaje si no lo está
	$web = mysqli_fetch_array(mysqli_query($conexion,"SELECT iduser,url,seeuptime_stats,seeuptime_downtime FROM webs WHERE ID='$ejecucion[0]'"));
	if ($web['seeuptime_stats'] == ""){$web['seeuptime_stats'] = "0,0";}

	$codeweb = http_status($web['url']);
		if($codeweb == 200){$a = "0";} //Todo correcto
		elseif($codeweb >= 400 && $codeweb < 500){$a = "1";} //Errores 4xx
		elseif($codeweb >= 500 && $codeweb < 600){$a = "1";} //Errores 5xx, servidor sobrecargado o apagado
		elseif($codeweb >= 300 && $codeweb < 400){$a = "1";} //Errores 3xx, pagina movida
		else{$a = "1";} //Otros fallos

	$stats = explode(",",$web['seeuptime_stats']);
	if ($a == 0){ //La web sigue perfecta
		$stats[0]++;
		$downtimearray = explode(",",$web['seeuptime_downtime']);
		if (count($downtimearray) > 1 && $downtimearray[2] != 1){ //Se arregló el problema de downtime
			$downtimearray[2] = 1;
			$datosusr = mysqli_fetch_array(mysqli_query($conexion, "SELECT nick,email FROM usuarios WHERE ID='$web[iduser]'")); //Conseguir el email del usuario
			enviarmail(htmlspecialchars($web['url']) , $datosusr['email'], 2); //Enviar un mail de que se esta disponible de nuevo (argumento 3 = 2)
		} 
	}
	if ($a != 0){ //Problemas
		$stats[1]++; //Añadir al downtime
		if ($web['seeuptime_downtime'] == ""){$web['seeuptime_downtime'] = time().",0,0";} //Si no habia nada en la variable de downtime aun..
		$downtimearray = explode(",",$web['seeuptime_downtime']);
		if ($downtimearray[2] == 0){ //Downtime en curso
			$downtimearray[1]++;
		}
		if ($downtimearray[2] != 0){ //Nuevo downtime
			$arr = time().",1,0,";
			$datosusr = mysqli_fetch_array(mysqli_query($conexion, "SELECT nick,email FROM usuarios WHERE ID='$web[iduser]'")); //Conseguir el email del usuario
			enviarmail(htmlspecialchars($web['url']) , $datosusr['email'], 1); //Enviar un mail de que se esta en downtime (argumento 3 = 1)

		}
	}
	$downtimearray = implode(",",$downtimearray);
	$web['seeuptime_downtime'] = limitecontar($downtimearray,30); //Limite de 30 posiciones, es decir, 10 registros de downtime, y recuperamos el string
	$web['seeuptime_stats'] = implode(",",$stats);
	$web['seeuptime_downtime'] = $arr . $web['seeuptime_downtime']; //Metemos lo nuevo en el array , $arr sera "" si no hay nada, no problem
	mysqli_query($conexion, "UPDATE webs set seeuptime_stats = '$web[seeuptime_stats]', seeuptime_downtime = '$web[seeuptime_downtime]' WHERE ID='$ejecucion[0]'");

	//usleep(500000); //Un pequeño descanso para no saturar mucho el server. Borrado porque ahora curl pausa la ejecución del script los 3 segundos ($wait)
}
?>