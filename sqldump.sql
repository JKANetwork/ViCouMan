-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `nick` varchar(30) COLLATE utf8_bin NOT NULL,
  `email` varchar(40) COLLATE utf8_bin NOT NULL,
  `passw` varchar(64) COLLATE utf8_bin NOT NULL,
  `accid` varchar(10) COLLATE utf8_bin NOT NULL COMMENT 'Random para verificar contador',
  `sesid` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `usuarios` (`ID`, `nick`, `email`, `passw`, `accid`, `sesid`) VALUES
(1,	'admin',	'admin@paginaweb.com',	'8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918',	'v0k9vb3vih',	'd76u891rvx');

DROP TABLE IF EXISTS `webs`;
CREATE TABLE `webs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `nombreweb` varchar(45) COLLATE utf8_bin NOT NULL,
  `url` varchar(90) COLLATE utf8_bin NOT NULL,
  `vistotales` int(11) NOT NULL DEFAULT '0',
  `visUtotales` int(11) NOT NULL DEFAULT '0',
  `vishoy` int(11) NOT NULL DEFAULT '0' COMMENT 'Visitas de hoy',
  `visUhoy` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Visitas unicas de hoy',
  `vismes` int(11) NOT NULL DEFAULT '0',
  `visUmes` int(11) NOT NULL DEFAULT '0',
  `vismeshist` varchar(170) COLLATE utf8_bin NOT NULL COMMENT 'Historial con "," del ultimo año',
  `visUmeshist` varchar(170) COLLATE utf8_bin NOT NULL COMMENT 'Historial con "," del ultimo año',
  `vishistorial` text COLLATE utf8_bin NOT NULL COMMENT 'Historial con ","',
  `visUhistorial` text COLLATE utf8_bin NOT NULL COMMENT 'Historial con ","',
  `huso` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Huso horario',
  `opc_browserest` tinyint(4) NOT NULL DEFAULT '0',
  `browserest` varchar(80) COLLATE utf8_bin NOT NULL DEFAULT '0,0,0,0,0,0,0,0,0,0' COMMENT 'OTHER,MOVIL,MSIE,EDGE,IE11,OPERA,FIRE,CHROME,SAFARI,BOT',
  `opc_countbot` tinyint(4) NOT NULL DEFAULT '0',
  `opc_seeuptime` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Hacer test de uptime',
  `seeuptime_stats` varchar(100) COLLATE utf8_bin NOT NULL COMMENT 'x,y(month uptime, month downtime)',
  `seeuptime_downtime` varchar(200) COLLATE utf8_bin NOT NULL COMMENT 'timestamp,how much time,finish',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 2017-01-02 23:25:41
