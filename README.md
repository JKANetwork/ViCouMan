# ViCouMan Visit Count Manager

Este es el codigo de nuestra web vicouman.com limpio (Sin datos personales), abierto al publico.

Es un ejemplo de como se puede hacer un gestor de visitas y estadísticas que puede ser para webs privadas
o para todo el mundo

Estamos abiertos a corregir bugs en el código que encontréis, o mejoras para no usar shell\_exec por ejemplo

## Requisitos

PHP

MySQL

Servidor con acceso a crones

## Instalacion

Primero descargar el git 

Copiar el archivo descomprimido al servidor

Hay que editar los siguientes archivos: basic.php, contadorsc.php,counterajax.php, y los de dentro de crones

En todos esos, las primeras dos lineas cambiarlas por un usuario, contraseña, y base de datos para el proyecto

Después, donde tengáis en el servidor la administración de crones, hay que añadir crones/cronhorario.php a ejecutar cada hora, y 
crones/cronuptimeweb.php a ejecutar cada 10 minutos

Como ultimo paso, importar con vuestro gestor de MySQL el archivo sqldump.sql en la base de datos que vayáis a usar

El usuario y contraseña por defecto es admin y admin