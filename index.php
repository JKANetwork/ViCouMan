<?php
require "basic.php"; //Requerido el connect, que incluye la conexion a la base de datos y sus funciones
basicboot();
headmenu();
?>
<div id="whitediv">
<p>Bienvenidos a Vicouman, el nuevo y rápido sistema para ver cuantas visitas tiene tu web unicas y totales, y que no necesita (ni recopila) datos más que los necesarios</p>
	<h3>¿Que ofrecemos?</h3>
	<ul>
		<li>Contador de visitas con estadísticas en tiempo real</li>
		<li>Muestreo de uptime y downtime de tu web, con emails! Reacciona rápido si tu web falla</li>
		<li>Una página con muchas utilidades para tu web</li>
		<li>Gráficas y tablas para todo, porque una grafica vale más que mil palabras</li>
		<li>Todo esto y más a dos clicks de ratón, y customizable con varias opciones</li>
	</ul>
	<h3 style="text-align:center;">¿A que esperas para probarnos?</h3>
	<p style="text-align:center;">¡Tendrás graficas y tablas como esta mostrandote los datos en tiempo real!</p>
	<table style="margin:0px auto;"><tr>
		<td>
	<canvas id="indexgraf" width="750" height="350" style="display: block;margin: 0 auto;"></canvas>
		</td>
		<td>
			Porcentaje de uptime de la web:<br>
	<canvas id="seeuptime" width="250" height="250" style="display: block;margin: 0 auto;"></canvas>
		</td></tr></table>
<p>Sabrás cuantas cargas de pagina hubo en tu web, y cuantas visitas únicas fueron. Así puedes pensar que hiciste y saber que le gusta a tu público (u otras cosas que se te ocurran)<br>
	También somos capaces de unir en ellas o no a los crawlers/buscadores, deciros su porcentaje, esquivarles, de forma fácil con opciones<br>
	Además tenemos varias utilidades para tu web en el menú, para que puedas dar rienda suelta a tu imaginación</p>
	Ejemplos de tipos de contadores, y también hay invisibles!
<br>
	
<div id="divVicouman"></div>
<script type="text/javascript">
var idweb=3;
var accid="v0k9vb3vih";
var mostrarU=0;
var estilo=5;
var pause=0;
</script>
<script src="https://vicouman.com/counter.js"></script>
	
<br>
</div>

	 <script>
    var ctx = document.getElementById("indexgraf").getContext("2d");
    var myChart = new Chart(ctx, {
	type: 'line',
	  data: {
      labels: ["20/04","21/04","22/04","23/04","24/04","25/04"],
      datasets: [{
        label: "Visitas", 
        backgroundColor: "rgba(100,176,243,0.2)", /* Color del sombreado de la grafica en ese lugar*/
        borderColor: "#4ba4f3", /* Colores de la linea y punteado */
        pointBorderColor: "#4ba4f3",
        pointBackgroundColor: "#fff",
        data: ["60","58","110","77","50","55"]
      },{
        label: "Visitas unicas",
        backgroundColor: "rgba(70,121,245,0.2)", /* Color del sombreado de la grafica en ese lugar*/
        borderColor: "#2661f1",/* Colores de la linea y punteado */
        pointColor: "#2661f1",
		pointBorderColor: "#2661f1",
 		pointBackgroundColor: "#fff",
        data: ["40","48","80","70","40","38"]
      }]
    }
	});
  </script>

	 <script>
    var ctxupa = document.getElementById("seeuptime").getContext("2d");
    var Chart2 = new Chart(ctxupa, {
	type: 'pie',
	data: {
	labels: ["Uptime","Downtime"],
	datasets: [
		{
			data: [65,35],
		backgroundColor: ["#35dd5d","#f10101"],
		hoverBackgroundColor: ["#53f179","#f53c3c"]
	}]
	}
	});
  </script>